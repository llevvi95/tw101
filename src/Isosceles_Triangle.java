import java.util.Scanner;

public class Isosceles_Triangle {

    public static void main (String args[]){
        Scanner scanner = new Scanner(System.in);
        String input;
        int length;
        System.out.print("Type the number");
        input = scanner.next();
        length = Integer.parseInt(input);

        int row = 1;
        int spaces = length-1;

        for (int i=0; i<length; i++){
            for (int s=0; s<spaces; s++){
                System.out.print(" ");
            }
            spaces--;
            for (int j=0; j<row; j++){
                System.out.print("*");
            }
            System.out.println("");
            row = row+2;
        }
    }
}
