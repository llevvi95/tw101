import java.util.Scanner;

public class right_triangle {

    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        String length;
        System.out.print("Type the length: ");
        length = scanner.next();

        for(int i=0; i<Integer.parseInt(length); i++){
            for(int j=1; j<i+2; j++){
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
