import java.util.Scanner;

public class draw_horizontal_line {

    public static void main(String args[]){
        String length;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Type the length of your line: ");

        length = scanner.next();

        for (int i = 0; i<Integer.parseInt(length); i++){
            System.out.print('*');
        }
    }
}
