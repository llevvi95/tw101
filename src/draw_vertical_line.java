import java.util.Scanner;

public class draw_vertical_line {

    public static void main(String args[]){
        String length;

        System.out.print("Please, type the line length:");
        Scanner scanner = new Scanner(System.in);

        length = scanner.next();

        for(int i =0; i<Integer.parseInt(length); i++){
            System.out.println('*');
        }

    }
}
