import java.util.Scanner;

public class diamond_triangle {

    public static void main (String args[]){
        Scanner scanner = new Scanner(System.in);
        String input;
        int length;

        System.out.print("Type the number: ");
        input = scanner.next();
        length = Integer.parseInt(input);
        int row = 1;
        int spaces;
        int s;

        spaces = length-1;
        for(int i=0; i<length; i++){
            for (s=0; s<spaces; s++){
                System.out.print(" ");
            }
            spaces--;

            for(int r=0; r<row; r++){
                System.out.print("*");
            }
            System.out.println();
            row = row+2;
        }

        spaces = 1;
        row = row-2;
        for (int i=length-1; i>0; i--){
            for (s=0; s<spaces; s++){
                System.out.print(" ");
            }
            spaces++;

            row = row-2;
            for(int r=0; r<row; r++){
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
